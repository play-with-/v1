# Load the Rails application.
require File.expand_path('../application', __FILE__)

require 'carrierwave/orm/activerecord'
require 'carrierwave'

# Initialize the Rails application.
Rails.application.initialize!
Rails.logger = Le.new('66e6622d-8266-4b41-88a2-1c345447f20a', :debug => true, :local => true)

ActiveRecord::Base.logger.level = Logger::DEBUG