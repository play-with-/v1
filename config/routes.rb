Rails.application.routes.draw do
  resources :messages
  resources :profilelinks
    root 'games#index' 
    
    get 'login' => 'login_sessions#new'
    get 'login' => 'login_sessions#create'
    get 'logout' => 'login_sessions#destroy'
    post 'login' => 'login_sessions#create'
	
	get 'signup' => 'users#new'
    
#    get 'what-is-it' => 'staticpages#what'
#    get 'pricing-payments' => 'staticpages#prices'
#    get 'security-support-help' => 'staticpages#security'
#    get 'explainer' => 'staticpages#explainer'
#    get 'hello' => 'staticpages#signuplanding'
    get 'privacy' => 'staticpages#privacy'
  	resources :games
    resources :users, :path => "players"
    get 'allparties' => 'users#allparties'
	resources :login_sessions
    resources :tags
    resources :parties do 
    resources :messages
    end
    resources :searches
#    resources :party_options
#    resources :donations
    resources :profilelinks
    
    get '*path' => redirect('/')
    
	# Add cookie creation to profile create

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
