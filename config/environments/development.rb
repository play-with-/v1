Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

    # This is for the sandbox server in mailgun
#    config.action_mailer.delivery_method = :smtp
#config.action_mailer.smtp_settings = {
#  :authentication => :plain,
#  :enable_starttls_auto => true,
#  :address => "smtp.mailgun.org",
#  :port => 587,
#  :domain => "sandbox6437cc43d36945fd8f2a3e8aa27444e0.mailgun.org",
#  :user_name => "postmaster@sandbox6437cc43d36945fd8f2a3e8aa27444e0.mailgun.org",
#  :password => "d9dc35d43dc6a639d4f3cddd0439906c"
#}
      config.action_mailer.delivery_method = :smtp
config.action_mailer.smtp_settings = {
  :authentication => :plain,
  :enable_starttls_auto => true,
  :address => "smtp.mailgun.org",
  :port => 587,
  :domain => "gathr.gg",
  :user_name => "postmaster@gathr.gg",
  :password => "3d8a64e02567928d50b5cf5f9b63130c"
    #Is this secure here?
}  
  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
    config.action_mailer.raise_delivery_errors = true

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end
