class AddStatusToParty < ActiveRecord::Migration
  def change
    add_column :parties, :status, :integer
  end
end
