class Donations < ActiveRecord::Migration
  def change
      create_table :donations do |t|
        t.integer :to_user_id
        t.integer :from_user_id
        t.string :amount
        t.string :percentage_charge
        t.string :status
      t.timestamps null: false
    end
  end
end
