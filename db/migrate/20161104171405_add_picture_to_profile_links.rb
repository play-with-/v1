class AddPictureToProfileLinks < ActiveRecord::Migration
  def change
      add_column :profilelinks, :image_url, :string
  end
end
