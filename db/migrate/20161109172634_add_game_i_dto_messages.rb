class AddGameIDtoMessages < ActiveRecord::Migration
  def change
              change_table :messages do |t|
                  t.references :game, index: true, foreign_key: true

  end
  end
end
