class CreatePartyOptions < ActiveRecord::Migration
  def change
    create_table :party_options do |t|
        t.string :name
        t.string :price
        t.string :content
        t.string :length
      t.timestamps null: false
    end
  end
end
