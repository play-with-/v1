class Usertoparty < ActiveRecord::Migration
  def change
      create_table :parties_users, id: false do |t|
          t.belongs_to :party, index: true
          t.belongs_to :user, index: true
      #drop_table :party_memberships
      end
  end
end
