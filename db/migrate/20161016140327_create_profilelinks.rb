class CreateProfilelinks < ActiveRecord::Migration
  def change
    create_table :profilelinks do |t|
            t.belongs_to :user,
            index: true
          t.string :name
          t.string :url
      t.timestamps null: false
    end
  end
end
