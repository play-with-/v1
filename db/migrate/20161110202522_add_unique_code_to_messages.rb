class AddUniqueCodeToMessages < ActiveRecord::Migration
  def change
      add_column :messages, :user_identifier, :string
  end
end
