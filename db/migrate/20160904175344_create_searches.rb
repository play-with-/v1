class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :game
      t.string :user_profile_tag

      t.timestamps null: false
    end
  end
end
