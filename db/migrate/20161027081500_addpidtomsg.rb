class Addpidtomsg < ActiveRecord::Migration
    def change
        change_table :messages do |t|
            t.references :party, index: true, foreign_key: true
        end
  end
end
