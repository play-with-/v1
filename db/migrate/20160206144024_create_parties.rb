class CreateParties < ActiveRecord::Migration
  def change
     # drop_table :parties
    create_table :parties do |t|
      t.string :length
        t.belongs_to :game, index: true
      t.timestamps null: false
    end
  end
end
