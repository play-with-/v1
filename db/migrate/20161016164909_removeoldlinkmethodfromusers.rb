class Removeoldlinkmethodfromusers < ActiveRecord::Migration
  def change
      remove_column :users, :website
      remove_column :users, :link_label
  end
end
