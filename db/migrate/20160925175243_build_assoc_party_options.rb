class BuildAssocPartyOptions < ActiveRecord::Migration
  def change
      add_reference :party_options, :user, foreign_key: true
      add_reference :party_options, :party, foreign_key: true
  end
end
