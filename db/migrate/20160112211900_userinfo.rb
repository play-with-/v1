class Userinfo < ActiveRecord::Migration
  def change
      change_table :users do |t|
          t.string :about
          t.string :website
      end
  end
end
