class RemoveOldTagMemberships < ActiveRecord::Migration
  def change
      drop_table :tag_memberships
  end
end
