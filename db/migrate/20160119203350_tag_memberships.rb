class TagMemberships < ActiveRecord::Migration
  def change
      create_table :tag_memberships do |t|
          t.belongs_to :tag, index: true
          t.belongs_to :user, index: true
      end
  end
end