# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161110202522) do

  create_table "donations", force: :cascade do |t|
    t.integer  "to_user_id",        limit: 4
    t.integer  "from_user_id",      limit: 4
    t.string   "price",             limit: 255
    t.string   "percentage_charge", limit: 255
    t.string   "status",            limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "name",              limit: 255
    t.string   "permalink",         limit: 255
  end

  create_table "games", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "slug",         limit: 255
    t.string   "game_picture", limit: 255
    t.string   "image_url",    limit: 255
  end

  create_table "games_users", id: false, force: :cascade do |t|
    t.integer "game_id", limit: 4
    t.integer "user_id", limit: 4
  end

  add_index "games_users", ["game_id"], name: "index_games_users_on_game_id", using: :btree
  add_index "games_users", ["user_id"], name: "index_games_users_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.text     "body",            limit: 65535
    t.integer  "user_id",         limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "party_id",        limit: 4
    t.integer  "game_id",         limit: 4
    t.string   "user_identifier", limit: 255
  end

  add_index "messages", ["game_id"], name: "index_messages_on_game_id", using: :btree
  add_index "messages", ["party_id"], name: "index_messages_on_party_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "parties", force: :cascade do |t|
    t.string   "length",           limit: 255
    t.integer  "game_id",          limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "party_option_id",  limit: 4
    t.integer  "previous_user_id", limit: 4
    t.datetime "datetime"
    t.string   "details",          limit: 255
    t.integer  "status",           limit: 4
  end

  add_index "parties", ["game_id"], name: "index_parties_on_game_id", using: :btree
  add_index "parties", ["party_option_id"], name: "fk_rails_0939cfb33e", using: :btree

  create_table "parties_users", id: false, force: :cascade do |t|
    t.integer "party_id", limit: 4
    t.integer "user_id",  limit: 4
  end

  add_index "parties_users", ["party_id"], name: "index_parties_users_on_party_id", using: :btree
  add_index "parties_users", ["user_id"], name: "index_parties_users_on_user_id", using: :btree

  create_table "party_options", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "price",      limit: 255
    t.string   "content",    limit: 255
    t.string   "length",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id",    limit: 4
    t.integer  "party_id",   limit: 4
  end

  add_index "party_options", ["party_id"], name: "fk_rails_a8875fd22f", using: :btree
  add_index "party_options", ["user_id"], name: "fk_rails_d23170adbf", using: :btree

  create_table "profilelinks", force: :cascade do |t|
    t.integer "user_id",   limit: 4
    t.string  "name",      limit: 255
    t.string  "url",       limit: 255
    t.string  "image_url", limit: 255
  end

  add_index "profilelinks", ["user_id"], name: "index_profilelinks_on_user_id", using: :btree

  create_table "searches", force: :cascade do |t|
    t.string   "game",             limit: 255
    t.string   "user_profile_tag", limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "signups", force: :cascade do |t|
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "email",      limit: 255
    t.string   "gamename",   limit: 255
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "display_name", limit: 255
    t.string   "icon",         limit: 255
  end

  create_table "tags_users", id: false, force: :cascade do |t|
    t.integer "tag_id",  limit: 4
    t.integer "user_id", limit: 4
  end

  add_index "tags_users", ["tag_id"], name: "index_tags_users_on_tag_id", using: :btree
  add_index "tags_users", ["user_id"], name: "index_tags_users_on_user_id", using: :btree

  create_table "user_income_details", force: :cascade do |t|
    t.integer  "user_id",                limit: 4
    t.string   "country",                limit: 255
    t.string   "disabled_reason",        limit: 255
    t.datetime "due_by"
    t.boolean  "company_account"
    t.string   "legal_first_name",       limit: 255
    t.string   "legal_last_name",        limit: 255
    t.datetime "dob"
    t.boolean  "tos_acceptance"
    t.string   "tos_ip",                 limit: 255
    t.string   "legal_address",          limit: 255
    t.string   "legal_address_line",     limit: 255
    t.string   "legal_address_postcode", limit: 255
  end

  add_index "user_income_details", ["user_id"], name: "index_user_income_details_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "password",        limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "password_digest", limit: 255
    t.string   "about",           limit: 255
    t.string   "allow_donation",  limit: 255
    t.string   "avatar",          limit: 255
    t.string   "longdesc",        limit: 255
    t.string   "reset_digest",    limit: 255
    t.datetime "reset_sent_at"
    t.string   "gamename",        limit: 255
  end

  add_foreign_key "messages", "games"
  add_foreign_key "messages", "parties"
  add_foreign_key "messages", "users"
  add_foreign_key "parties", "party_options"
  add_foreign_key "party_options", "parties"
  add_foreign_key "party_options", "users"
end
