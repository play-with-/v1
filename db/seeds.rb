Game.create(name: "Dota 2", slug: "dota-2", image_url: "https://s3-eu-west-1.amazonaws.com/obscure-river-53311/Dota_2-272x380.jpg")
Game.create(name: "CS:GO", slug: "cs-go")
Game.create(name: "Minecraft", slug: "minecraft")
Game.create(name: "League of Legends", slug: "lol")
Tag.create(name: "Learn", display_name: "Learn", icon: "glyphicon glyphicon-education")
Tag.create(name: "Fun", display_name: "Have fun", icon: "glyphicon glyphicon-star")
Tag.create(name: "Find teammates", display_name: "Find new team mates", icon: "glyphicon glyphicon-search")