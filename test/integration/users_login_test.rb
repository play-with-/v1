require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
   test "login with invalid information" do
    get login_path
    assert_template 'login_sessions/new'
    post login_path, login_session: { email: "", password: "" }
    assert_template 'login_sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
end
