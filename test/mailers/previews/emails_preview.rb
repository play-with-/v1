# Preview all emails at http://localhost:3000/rails/mailers/emails
class EmailsPreview < ActionMailer::Preview
    def welcome_mail_preview
        Emails.welcome_email(User.first)
  end
end