require 'test_helper'

class ProfilelinksControllerTest < ActionController::TestCase
  setup do
    @profilelink = profilelinks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:profilelinks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create profilelink" do
    assert_difference('Profilelink.count') do
      post :create, profilelink: {  }
    end

    assert_redirected_to profilelink_path(assigns(:profilelink))
  end

  test "should show profilelink" do
    get :show, id: @profilelink
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @profilelink
    assert_response :success
  end

  test "should update profilelink" do
    patch :update, id: @profilelink, profilelink: {  }
    assert_redirected_to profilelink_path(assigns(:profilelink))
  end

  test "should destroy profilelink" do
    assert_difference('Profilelink.count', -1) do
      delete :destroy, id: @profilelink
    end

    assert_redirected_to profilelinks_path
  end
end
