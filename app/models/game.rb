class Game < ActiveRecord::Base
    has_and_belongs_to_many :users
    has_many :messages

    def to_param
        slug
    end
    
    mount_uploader :game_picture, GamePictureUploader
end