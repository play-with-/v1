class Profilelink < ActiveRecord::Base
    before_validation :smart_add_url_protocol
    belongs_to :user
    
    validates :user_id, presence: true

        
protected
    def smart_add_url_protocol
        puts ">>>"
        puts "running url checker"
        unless self.url.blank?
        unless self.url[/\Ahttp:\/\//] || self.url[/\Ahttps:\/\//]
      self.url = "http://#{self.url}"
  end
  end
end
    
end
