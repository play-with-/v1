class User < ActiveRecord::Base
    mount_uploader :avatar, AvatarUploader
    
    validates_processing_of :avatar
validate :image_size_validation
            
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
    validates :name, presence: true, :length => { :within => 3..30}
    validates :email, presence: true, uniqueness: true, length: { maximum: 50 }
    
    validates :about, length: { maximum: 80 }
    validates :longdesc, length: { maximum: 175 }
    
    has_and_belongs_to_many :games
    has_and_belongs_to_many :tags
    has_and_belongs_to_many :parties
    has_many :party_options
    has_many :donations
    has_many :messages
    
    has_many :profilelinks
    accepts_nested_attributes_for :profilelinks
    
private
  def image_size_validation
    errors[:avatar] << "should be less than 500KB" if avatar.size > 0.5.megabytes
  end

end