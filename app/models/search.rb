class Search < ActiveRecord::Base
    
    def users
    
#        users_g = User.includes(:games).where("games.name LIKE ?", game)  
        users_g = User.includes(:games).where('games.name LIKE ?', "%#{game}%")
#        users_g = User.includes(:games).where("games.name", game)
        
        users_t = User.includes(:tags).where("tags.name" => user_profile_tag)
        
        #       based on search terms present, add the two users or just set them accodrdingly. There won't be a condition where both are blank, as the controller checks for that.
        
        if user_profile_tag.present? && game.present?
            users = users_g.merge(users_t)
#            puts '>>>>'
#            puts 'both search terms return users'
        else
            if game.present?
                users = users_g
#                puts '>>>>'
#                puts 'only games present'        
            end
            
            if user_profile_tag.present?
                users = users_t
#                puts '>>>>'
#                puts 'only tags present'    
            end
        end
        
        return users
                
    end
    
end

#       debug if required : move to funct above display the search criteria
#        
#        puts '*** the game = '
#        puts game
#        
#        puts '*** the tag = '
#        puts user_profile_tag
#        
#               
#        puts '***** USERS with search tag >>'
#        users_t.each do |mlep|
#            puts mlep.name
#        end
#        
#        
#        puts '*** >> USERS with search game'
#        users_g.each do |slep|
#            puts slep.name
#        end
#        puts '***'
#
#       more debug, if ya need it        
#        puts 'results:'
#        users.each do |cka|
#            puts cka.name
#        end
#        puts '<<<'
#        
