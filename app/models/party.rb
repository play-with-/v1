class Party < ActiveRecord::Base
    has_and_belongs_to_many :users, :validate => false
    belongs_to :game
    belongs_to :party_option
    has_many :messages
    
#    users = User.all
end
