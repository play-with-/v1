document.addEventListener("turbolinks:load", function() {
    // game selection on search
$(".light, .game-label-cover").on("click", function(){
    var id = $(this).attr('id');
    $('div[id^="game-panel"]').css({'opacity':''})
    $('[id^="selected-status"]').removeClass( "glyphicon glyphicon-ok", 1000, "easeOut" );
    
    //de-select checkbox here too?
    
    $("#game-panel"+id).css({'opacity':'0.7'});
    $("#selected-status"+id).addClass("glyphicon glyphicon-ok");
    
});
    // tag selection on new search
    $(".tag-name, .tag-label-cover").on("click", function(){
    var id = $(this).attr('id');
    $('div[id^="tag-panel"]').css({'opacity':''})
    $('[id^="tag-selected-status"]').removeClass( "glyphicon glyphicon-ok", 1000, "easeOut" );
        
    //de-select checkbox here too?
        
    $("#tag-panel"+id).css({'opacity':'0.7'});
    $("#tag-selected-status"+id).addClass("glyphicon glyphicon-ok");
    
});
    // game selection - clear any prior selection on text box input
    $(".text-entry").on("click", function(){
$('.gamechoice-invis-checkbox').prop('checked', false); // Unchecks box
    $(".game-selector").css({'opacity':''})
    $(".game-select-span").removeClass( "glyphicon glyphicon-ok", 1000, "easeOut" );
        
    });
    
    
    //
    var divs = $('div[id^="u-option-"]').hide(),
    i = 0;

(function cycle() { 

    divs.eq(i).fadeIn(400)
              .delay(1400)
              .fadeOut(400, cycle);

    i = ++i % divs.length;

})();
    var maxLength = 80;
$('textarea').keyup(function() {
  var length = $(this).val().length;
  var length = maxLength-length;
  $('#chars').text(length);
});
    $( "#submitmessage" ).submit(function() {
//        alert("yo");
    $("#newmessage").val("");
});
});