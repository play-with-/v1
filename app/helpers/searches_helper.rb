module SearchesHelper
    def list_games
        gamearray = Array.new
        Game.find_each do |c|
        gamearray << c
        end
        return gamearray
    end

    def list_user_tags
        tagsarray = Array.new
        Tag.find_each do |z|
        tagsarray << z
        end
        return tagsarray
    end
end