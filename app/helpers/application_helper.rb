module ApplicationHelper
    def title_suffix
        @title_suffix = "| Gathr.gg"
    end
    
    def use_alternate_header? 
        controller.controller_name == "searches" && controller.action_name == "new" ||
            controller.controller_name == "games" && controller.action_name == "index"
        
        
        # || controller.controller_name == "staticpages"
end
    
    def no_footer_page?
        controller.controller_name == "staticpages"
        controller.controller_name == "searches" && controller.action_name == "new"
    end
    
        def is_admin?
            logged_in? && current_user.id == 1 
    end
    
end
