module PartiesHelper
    
    def party_option_lookup
        party_option_lookup = PartyOption.find_by_id(params[:party_option_id])
        
        return party_option_lookup
    end
    
    def previous_user_lookup
        previous_user_lookup = User.find_by_id(params[:previous_user])
        
        return previous_user_lookup
    end
    
    def common_games 
        
        if previous_user_lookup.blank?
            
        else
        
        common_games = 
        current_user.games.each && previous_user_lookup.games.each 
        end
            
        return common_games
    end
            
    def user_list
        user_list = User.find_by(@party.users) 
    end
end
