module LoginSessionsHelper
    #logs in the given user
    def log_in(user)
        session[:user_id] = [user.id]
    end
    
    #returns the current logged-in user (if any)
    def current_user		
		@current_user ||= User.find_by(id: session[:user_id])
		
		return unless cookies.signed[:permanent_user_id] || session[:user_id]
		begin
			@current_user ||= User.find(cookies.signed[:permanent_user_id] || session[:user_id])
			rescue Mongoid::Errors::DocumentNotFound
			nil
		end
    end

    # returns true if the user is logged in, false otherwise. Used for making functions appear only if logged in etc
    def logged_in? 
        !current_user.nil?
    end
    #
    def log_out
        session.delete(:user_id)
        @current_user = nil
    end
     # Redirects to stored location (or to the default).
    def redirect_back_or()
        redirect_to(session[:forwarding_url] || :root)
    session.delete(:forwarding_url)
  end
    
  # Stores the URL trying to be accessed.
    def store_location #move to models?
    session[:forwarding_url] = request.url if request.get?
  end
end
