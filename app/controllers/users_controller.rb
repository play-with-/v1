class UsersController < ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]
 	before_action :logged_in_user, only: [:edit, :update]
      before_action :correct_user,   only: [:edit, :update]

  # GET /users
  # GET /users.json
  def index
      @users = User.search(params[:search])
  end

  # GET /users/1
  # GET /users/1.json
  def show
      set_user
  end
    
    def allparties
        @user = current_user
        render :allparties 
    end

  # GET /users/new
  def new
	  if logged_in? 
#		  redirect_to current_user
		  respond_to do |format|
              flash[:warning] = "You already have an account. Here it is:"
              format.html {redirect_to current_user}
			format.json { render :show, status: :created, location: current_user }
		  end
		  # Maybe throw an error here?
	  else
		  @user = User.new
	  end
  end

  # GET /users/1/edit
  def edit
      set_user
  end

	# POST /users
	# POST /users.json
	def create
		if logged_in?
			respond_to do |format|
		  	format.html { redirect_to current_user, notice: 'You\'re already logged in mate. ' }
			format.json { render :show, status: :created, location: current_user }
		  end
		else
			@user = User.new(user_params)
		@user.email = @user.email.downcase
		
		respond_to do |format|
            if @user.save  
 ExampleMailer.sign_up_welcome(@user).deliver_now
				format.html { redirect_back_or 
                    }
            
				format.json { render :show, status: :created, location: @user }
                flash[:success] = "Welcome!"
				log_in @user
			else
				format.html { render :new }
				format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
		end
		
	end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        
        format.html { redirect_to @user }
          flash[:success] = "Nice, updates made"
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
        @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
        params.require(:user).permit(:name, :email, :password, :avatar, :allow_donation, :income, :longdesc, :about, profilelinks_attributes: [:id, :name, :url], game_ids: [], tag_ids: [], party_options_ids: [],  game_memberships_attributes: [ :id, :game_id ],  party_options_attributes: [ :id, :content, :name, :price, :length ],  user_tags_attributes: [ :id, :tag_id ])
    end
    #confirms a logged-in user
        def logged_in_user
      unless logged_in?
          store_location #links to helper function
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
        # Confirms the correct user. Make this a concerns?
    def correct_user
    unless @user == current_user
      #@user = User.find(params[:id])
        redirect_to(root_url, notice: 'That was an error! Submit an issue here: <provide a link>' ) 
        #unless @user == current_user
        #change root_url to the stored_location function??
    end
    end
end
