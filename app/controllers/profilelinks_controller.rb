class ProfilelinksController < ApplicationController
  before_action :set_profilelink, only: [:show, :edit, :update, :destroy]

  # GET /profilelinks
  # GET /profilelinks.json

  # GET /profilelinks/1
  # GET /profilelinks/1.json
  def show
  end

  # GET /profilelinks/new
  def new
    @profilelink = Profilelink.new
  end

  # GET /profilelinks/1/edit
  def edit
  end

  # POST /profilelinks
  # POST /profilelinks.json
  def create
    @profilelink = Profilelink.new(profilelink_params)

    respond_to do |format|
      if @profilelink.save
          format.html { redirect_to user_path(id: (@profilelink.user.id))}
        format.json { render :show, status: :created, location: @profilelink }
          
          flash[:success] = "Created a link."
          
      else
        format.html { render :new }
        format.json { render json: @profilelink.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profilelinks/1
  # PATCH/PUT /profilelinks/1.json
  def update
  end

  # DELETE /profilelinks/1
  # DELETE /profilelinks/1.json
  def destroy
    @profilelink.destroy
    respond_to do |format|
      format.html { redirect_to user_path(id: (@profilelink.user.id))}
          
        flash[:warning] = "Deleted link."
        
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profilelink
      @profilelink = Profilelink.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profilelink_params
        params.require(:profilelink).permit(:url, :name, :user_id).merge(user_id: current_user.id)
    end
end
