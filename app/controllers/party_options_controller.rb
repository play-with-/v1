class PartyOptionsController < ApplicationController
    
      before_action :set_party_option, only: [:show, :edit, :update, :destroy]
    before_action :logged_in_user, only: [:show] #also include edit and update?    
    
    
    
    #before_action :logged_in_user, only: [:edit, :update]
    
    #Ensure only users can edit their own party option!
    
    def new
        @party_option = PartyOption.new
    end
    
    def create
        @party_option = PartyOption.new(party_option_params)
        
        if @party_option.save!
        redirect_to @party_option
        else
        format.html { render :new }
        format.json { render json: @party_option.errors, status: :unprocessable_entity } 
        end
    end
    
    def show
    end
    
    def edit
        set_party_option
    end
    
      def update
    respond_to do |format|
      if @party_option.update(party_option_params)
        format.html { redirect_to @user }
          flash[:success] = "Nice, updates made"
          format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @party_option.errors, status: :unprocessable_entity }
      end
    end
  end
    
  def destroy
    @party_option.destroy
    respond_to do |format|
        format.html { redirect_to user_path(current_user) }
        format.json { head :no_content }
            flash[:info] = "Deleted party option."
    end
  end

    private

    def set_party_option
    @party_option = PartyOption.find(params[:id])
    end
    
    
    def party_option_params
        params.require(:party_option).permit(:name, :price, :content, :length, :party, :user_id).merge(user_id: current_user.id)
    end

       def logged_in_user
      unless logged_in?
          store_location #links to helper function
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    def correct_user
        #something here to only match the owner
    end
end
