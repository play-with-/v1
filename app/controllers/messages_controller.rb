class MessagesController < ApplicationController
#  before_action :set_message, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js


  # GET /messages
  def index
    @messages = Message.all
    @new_message = current_user.messages.build
  end



    def create
#        user_management
      @message = Message.new(message_params)
    if @message.save
        sync_new @message
    end
    respond_with { @message }
end


  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
    
#    def user_management
#        if current_user.present?
#            params[:theuser] = current_user.id
#        elsif params[:user_identifier].nil?
#            params.merge(user_identifier: params[:anon_user_id])
#            puts ">>>>"
#            puts params[:user_identifier]
#    end
#end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
                
        params.require(:message).permit(:body, :user_id, :party_id, :game_id, :user_identifier).merge(game_id: params[:thegame], party_id: params[:theparty], user_id: params[:theuser], user_identifier: params[:anon_user_id])
    end
end
