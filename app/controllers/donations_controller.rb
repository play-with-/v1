class DonationsController < ApplicationController
    before_action :set_donation, only: [:show, :edit, :update, :destroy]


  def index
    @donations = Donation.all
  end

  def show
  end

  def new
    @donation = Donation.new
  end


  def edit
  end


  def create
      if logged_in?
          @donation = Donation.new(donation_params)
          
          respond_to do |format|
          if @donation.save
              format.html { redirect_to @donation, notice: 'Donation successful! Thanks for supporting.' }
        format.json { render :show, status: :created, location: @donation }
      else
        format.html { render :new }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
       end       
      end

  else
        respond_to do |format|
            format.html { redirect_to signup_path, notice: 'Log in or sign up before making a donation!' }
			format.json { render :show, status: :created, location: current_user }
        end
  end #end if logged in 
end #end create
  def update
    respond_to do |format|
      if @donation.update(donate_params)
        format.html { redirect_to @donation, notice: 'Donation was successfully updated.' }
        format.json { render :show, status: :ok, location: @donation }
      else
        format.html { render :edit }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_donation
      @donation = Donation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def donate_params
        params.require(:donation).permit(:id, :to_user_id, :from_user_id, :amount, :percentage_charge, :status)
    end
end
