class LoginSessionsController < ApplicationController
	def new
        @user = User.new
	end
	
	def create
		user = User.find_by(email: params[:login_session][:email].downcase)	
		
		if user && user.authenticate(params[:login_session][:password])
		
		    if params[:remember_me]
 cookies.permanent.signed[:permanent_user_id] = user.id
            end
            
			log_in user
			redirect_back_or
            flash[:success] = "Welcome!"
		else
			flash.now[:danger] = 'Wrong email or password'
			render 'new'
		end
	end
    
    def both
        
    end
    
    
	def destroy
		cookies.permanent.signed[:permanent_user_id] = nil
		log_out
		redirect_to root_url
        flash[:warning] = "Logged out. See you soon!"
	end
end