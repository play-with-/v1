class GamesController < ApplicationController
  before_action :set_game, only: [:show, :edit, :update, :destroy]

  # GET /games
  # GET /games.json
  def index
      
          @games = Game.paginate(:page => params[:page], :per_page => 15)
  end

  # GET /games/1
  # GET /games/1.json
  def show
#      @new_message = current_user.messages.build
      
      @game = Game.find_by_slug(params[:id])
      @new_message = @game.messages.build

      respond_to do |format|
          format.html # show.html.erb
          format.json { render json: @game }
  end
end


  # GET /games/new
  def new
		@game = Game.new
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)
      @game.slug =  @game.name.downcase.gsub(" ", "-")
	  
    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
		  
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
        # looks for matching slugs and returns the game
        @game = Game.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
        params.require(:game).permit(:name, :game_picture, :image_url, :slug)
    end
end
