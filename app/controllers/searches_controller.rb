class SearchesController < ApplicationController

    def new
        @search = Search.new
        @game = User.includes(:game).where(game: { name: @game })
        @user_profile_tag = User.includes(:tag).where(tag: { name: @user_profile_tag })
                
    end
    def create
        if params[:search].nil?
            flash[:danger] = "Oops! Please choose an option"
        return redirect_to root_path(anchor: 'message')
        end
        
        @search = Search.create(search_params)
        redirect_to @search
    end
    
        
    def show
        @search = Search.find(params[:id])
        @search_users = @search.users.all.paginate(:page => params[:page], :per_page => 15)
    end
    
    def index
        @search = Search.paginate(:page => params[:page])
    end

    private
    
    def search_params
        params.require(:search).permit(:game, :user_profile_tag)
    end
end