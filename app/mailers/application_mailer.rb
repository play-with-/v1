class ApplicationMailer < ActionMailer::Base
    default from: 'message@gathr.gg'
  layout 'mailer'
end